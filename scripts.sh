#!/usr/bin/env bash

set -eu

function prepare_env {
    set -o allexport

    if [ ! -f "docker-compose.env" ]; then
      echo "docker-compose.env does not exists"
      echo "aborting"

      exit 1
    fi

    source docker-compose.env

    [ -f "docker-compose.env.local" ] && source docker-compose.env.local

    set +o allexport
}

function docker_compose {
    docker-compose $@
}

function pull_images {
    docker_compose pull
}

function start_services {
    docker_compose up --build
}

function stop_services {
    docker_compose stop
}

function run_php {
    docker_compose exec app $@
}

function run_composer {
    run_php composer $@
}

function run_console {
    run_php bin/console $@
}

function run_symfony {
    run_php /usr/local/bin/symfony $@
}

function connect_mysql_shell {
    docker exec -ti mysql-8 mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE}
}

function reset_db {
    run_console doctrine:database:drop --force && \
    run_console doctrine:database:create && \
    run_console doctrine:migrations:migrate --no-interaction --quiet && \
    run_console doctrine:fixtures:load --no-interaction --quiet --group=dev
}

command_full=$@
command_name="$1"
command_args=${command_full#"$command_name"}

echo "command full:   scripts.sh ${command_full}"
echo "command name:   ${command_name}"
echo "command args:  ${command_args}"
echo "command output: "
echo ""

(
prepare_env

case ${command_name} in
    docker-compose)
        docker_compose ${command_args}
        ;;
    start)
        pull_images
        start_services
        ;;
    stop)
        stop_services
        ;;
    down)
        down_services
        ;;
    php)
        run_php ${command_args}
        ;;
    composer)
        run_composer ${command_args}
        ;;
    console)
        run_console ${command_args}
        ;;
    symfony)
        run_symfony ${command_args}
        ;;
    phpunit)
        run_phpunit ${command_args}
        ;;
    robo)
        run_robo ${command_args}
        ;;
    mysql)
        connect_mysql_shell
        ;;
    db:reset)
      reset_db
      ;;
    reset:all)
        reset_redis
        reset_rabbitmq
        reset_db
        ;;
    reset:local)
        reset_db "--fixture=essentials"
        reset_rabbitmq
        ;;
    *)
        echo "${command_name} is unknown"
        exit 1
        ;;
esac
)
