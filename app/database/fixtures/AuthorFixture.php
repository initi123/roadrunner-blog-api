<?php

declare(strict_types = 1);

namespace App\Fixtures;

use App\Entity\Author;
use App\Enums\ArticleLanguageEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AuthorFixture extends Fixture implements FixtureGroupInterface
{
    public const MIN_OBJECTS           = 1;
    public const MAX_OBJECTS           = 5;
    public const REFERENCE_NAME_FORMAT = '%s-author-%d';

    /**
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

    public static function doReferenceName(ArticleLanguageEnum $languageEnum, int $pointer): string
    {
        return sprintf(self::REFERENCE_NAME_FORMAT, $languageEnum->value, $pointer);
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        $ruFaker = Factory::create('ru_RU');
        $enFaker = Factory::create('en_US');
        $esFaker = Factory::create('en_ES');

        for ($i = self::MIN_OBJECTS; $i <= self::MAX_OBJECTS; $i++) {
            $this->loadAuthor($ruFaker, $manager, self::doReferenceName(ArticleLanguageEnum::Russian, $i));
        }

        for ($i = self::MIN_OBJECTS; $i <= self::MAX_OBJECTS; $i++) {
            $this->loadAuthor($enFaker, $manager, self::doReferenceName(ArticleLanguageEnum::English, $i));
        }

        for ($i = self::MIN_OBJECTS; $i <= self::MAX_OBJECTS; $i++) {
            $this->loadAuthor($esFaker, $manager, self::doReferenceName(ArticleLanguageEnum::Spanish, $i));
        }

        $manager->flush();
    }

    private function loadAuthor(Generator $faker, ObjectManager $manager, string $reference): void
    {
        $author = new Author();
        $author->setFirstName($faker->firstName());
        $author->setLastName($faker->lastName());

        $manager->persist($author);

        $this->addReference($reference, $author);
    }
}
