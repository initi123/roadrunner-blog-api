<?php

declare(strict_types = 1);

namespace App\Fixtures;

use App\Entity\Article;
use App\Entity\Author;
use App\Enums\ArticleLanguageEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use RuntimeException;

use function rand;

class ArticleFixture extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        $ruFaker = Factory::create('ru_RU');
        $enFaker = Factory::create('en_US');
        $esFaker = Factory::create('en_ES');

        for ($i = 0; $i < 25; $i++) {
            $this->loadArticle($ruFaker, $manager, ArticleLanguageEnum::Russian);
        }

        for ($i = 0; $i < 25; $i++) {
            $this->loadArticle($enFaker, $manager, ArticleLanguageEnum::English);
        }

        for ($i = 0; $i < 25; $i++) {
            $this->loadArticle($esFaker, $manager, ArticleLanguageEnum::Spanish);
        }

        $manager->flush();
    }

    private function loadArticle(Generator $faker, ObjectManager $manager, ArticleLanguageEnum $languageEnum): void
    {
        $article = new Article();

        $article->setAuthor($this->getRandomAuthor($languageEnum));
        $article->setTitle($faker->realText(254));
        $article->setDescription($faker->realText());
        $article->setLanguage($languageEnum->value);

        $manager->persist($article);
    }

    private function getRandomAuthor(ArticleLanguageEnum $languageEnum): Author
    {
        $pointer = rand(AuthorFixture::MIN_OBJECTS, AuthorFixture::MAX_OBJECTS);
        $author  = $this->getReference(AuthorFixture::doReferenceName($languageEnum, $pointer));

        if (!$author instanceof Author) {
            throw new RuntimeException('$author must be of type ' . Author::class);
        }

        return $author;
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            AuthorFixture::class
        ];
    }

    /**
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }
}
