<?php

declare(strict_types = 1);

namespace App\Enums;

enum ArticleLanguageEnum: string
{
    public const RUSSIAN = 'Russian';
    public const ENGLISH = 'English';
    public const SPANISH = 'Spanish';

    public const ALL_VALUES = [
        self::RUSSIAN,
        self::ENGLISH,
        self::SPANISH,
    ];

    case Russian = self::RUSSIAN;
    case English = self::ENGLISH;
    case Spanish = self::SPANISH;
}
