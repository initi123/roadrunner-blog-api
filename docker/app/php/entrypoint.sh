#!/usr/bin/env sh

set -eux

install_packages () {
    composer install --prefer-dist --no-progress --no-suggest --no-interaction --no-cache
}

install_roadrunner () {
    if [ ! -e "/srv/app/bin/rr" ]; then
      /srv/app/vendor/bin/rr get --location /srv/app/bin
    fi
}

#wait_database () {
#    echo "DB: Waiting for db to be ready..."
#    until bin/console doctrine:query:sql "SELECT 1" > /dev/null 2>&1; do
#        sleep 1
#    done
#    echo "DB: Is up!"
#    ./bin/console doctrine:migrations:migrate --no-interaction
#    ./bin/console doctrine:fixtures:load --no-interaction --quiet
#}

install_packages
install_roadrunner
#wait_database

exec rr serve -c .rr.dev.yaml --debug
